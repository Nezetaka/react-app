resource "k8s_deployment" "react-app" {
    metadata {
        name = "react-app"
    }
    spec {
        selector {
            match_labels = {
                "app" = "react-app"
            }
        }
        replicas = 1
        template {
            metadata {
                labels = {
                    "app" = "react-app"
                }
            }
            spec {
                container {
                    name = "react-app"
                    image = "nezetaka/react-app"
                    port {
                        container_port = 80
                    }
                }
            }
        }
    }
}
